
const fs = require("fs");
const path = require("path");
const { execSync } = require("child_process");

module.exports = {
	readFileEsf: (file) => {
		let result = [];

		const dptValidate = (raw) => {
			if (raw.split(" ").length > 4) {
				return { dpt: raw.split(" ")[1], unit: raw.split(" ")[2].replace("'", "") }
			} else {
				return { dpt: raw.split(" ")[0].replace("'", ""), unit: raw.split(" ")[1].replace("(", "").concat(" ", raw.split(" ")[2].replace(")", "")) }
			}
		}

		file.split("\n").map((element, index) => {
			if (index == 0) { return }
			const line = element.split("\t");
			const obj = {
				mainLabel: line[0].split(".")[0],
				secondLabel: line[0].split(".")[1],
				dg: line[0].split(".")[2],
				dgName: line[1],
				...dptValidate(line[2])
			}

			result.push(obj);
		});

		return result;
	},
	readEsfFromFolder: (name) => {
		const location = execSync("echo $HOME");
		if (location) {
			try {
				const data = fs.readFileSync(`${location.toString().replace(/\n/, "")}/.node-red/source_register/${name}.json`);
				return JSON.parse(data.toString());
			} catch (error) {
					// console.log("READ ERROR: ", error);
				return null;
			}

		}
	},
	validateEsfData: (data) => {
		try {
			const { list } = data;
			const esf = [];
			Object.entries(list).map(([key, value]) => {
				esf.push(value);
			})
			return esf;
		} catch (error) {
			return null;
		}
	},
	valueToInt: (value) => {
		const raw = value.toString('hex');
		const result = parseInt(raw, 16);
		return result;
	},
	valueToString: (value) => {
		return value.toString('hex');
	},
	convertValue: (val) => {
		switch (typeof val) {
			case 'number':
				if (val < 9) {
					return `0${val.toString()}`;
				}
				return val.toString();
				break;
			case 'string':
				if (parseInt(val) < 9) {
					return `0${val}`;
				}
				return val;
				break;
		}
	},
	updateNodeStatus: (node, config) => {
		const { fill, shape, text } = config;
		node.status({
			fill: fill,
			shape: shape,
			text: text
		})
	},
	convertValueToHex: (value) => {
		if (typeof value == 'number') {
			return Buffer.from(value.toString(), 'hex');
		}
		return Buffer.from(value, 'hex');
	},
	decodeValue: (value) => {
		const raw = value.toString(value, 'hex');
		const result = parseInt(raw, 16);
		return result;
	},
	validateNumber: (raw) => {
		if (typeof raw == 'string') {
			return parseInt(raw);
		} else if (typeof raw == 'number') {
			return raw;
		} else {
			console.log("INVALID INCOMING VALUE TO WRITE");
			return null;
		}
	}
}