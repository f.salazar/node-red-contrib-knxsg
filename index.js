module.exports = (RED) => {

  const knx = require('knx');
  const knxDatapoints = require('knx-datapoints')

  const { readEsfFromFolder, validateEsfData, valueToInt, valueToString } = require("./tools");

  function knxControllerNode(config) {

    RED.nodes.createNode(this, config);

    this.config = config;
    this.config.ipAddr = this.config.ipAddr || '224.0.23.12';
    this.config.ipPort = this.config.ipPort || 3671;
    this.config.physAddr = this.config.physAddr || '15.15.15';
    this.config.incomingWrite = this.config.incomingWrite || false;
    const file = readEsfFromFolder(this.id);
    this.esfdata = file ? file.list : null;

    const getStatus = (status, props) => {
      switch (status) {
        case "start":
          return {
            fill: "yellow",
            text: "connecting",
            shape: "dot"
          }
          break;
        case "stop":
          return {
            fill: "gray",
            text: `closed`,
            shape: "dot",
          }
          break;
        case "connected":
          return {
            fill: "green",
            text: "connected",
            shape: "dot"
          }
          break;
        case "traffic":
          return {
            color: 'blue',
            text: `event: ${props.event} to ${props.dest} value: ${props.value}`,
            shape: 'dot'
          }
          break;
        case "error":
          return {
            color: 'red',
            text: `Node: ${props.node}, Error: ${props.error}`,
            shape: 'dot'
          }
          break;
        default:
          return {
            fill: "grey",
            text: `Status error`,
            shape: "dot",
          }
          break;
      }
    };

    const statusHandler = (event, status, statusProps, prevStatus = undefined, timeout = 2000) => {
      this.emit(event, getStatus(status, statusProps));
      if (prevStatus) {
        setTimeout(() => {
          this.emit(event, getStatus(prevStatus, statusProps));
        }, timeout);
      }
    };

    this.emit("status", getStatus("start"));

    this.interface = new knx.Connection({
      ...this.config,
      handlers: {
        connected: () => {
          this.emit("status", getStatus("connected"));
        },
        event: (evt, src, dest, value) => {

          const payload = {
            event: evt,
            srcphy: src,
            dstgad: dest,
          }

          if (evt.match(/GroupValue_(.*)/) && (evt == "GroupValue_Write" || evt == "GroupValue_Response")) {
            //DECODE VALUE
            if (this.esfdata) {
              const element = this.esfdata.find(e => e.dg === payload.dstgad);
              if (element) {
                payload.value = knxDatapoints.decode(element.dpt, value);
              }else{
                payload.dpt = undefined;
                payload.value = value;
                payload.hex = value.toString('hex');
              }
            } else {
              payload.dpt = undefined;
              payload.value = value;
              payload.hex = value.toString('hex');
            }

            statusHandler("traffic", "traffic", { event: evt.split("_")[1].toLowerCase(), dest, value: value.toString('hex') }, "connected")

            this.emit("data", { topic: "incomming", payload: payload });
          }
        },
        error: (error) => {
          if (!error.match(/(CONNECTIONSTATE_RESPONSE)/g)) {
            this.emit("error", getStatus("error", { node: this.id, error }));
          }
        }, disconnected: () => {
          //this.emit("disconnected", { fill: "red", shape: "dot", text: "disconnected" })
        }
      }
    })

    this.on("close", () => {
      this.emit("status", getStatus("stop"));
      this.interface.Disconnect();
      this.interface.removeAllListeners();
    })

  }

  function knxNode(config) {

    RED.nodes.createNode(this, config);

    const statusDecode = (raw) => {
      return {
        message: raw.text,
        title: "Status",
        variant: raw.fill.replace("red", "error").replace("gray", "info").replace("green", "success").replace("yellow", "warning"),
        id: this.id
      }
    }

    const globalEmitter = (data) => {
      emitter = globalContext.get(this.config.externalEvents.name);
      emitterChannel = this.config.externalEvents.channel;
      emitterEvent = this.config.externalEvents.event;
      emitter.emit(emitterChannel, { event: emitterEvent, args: statusDecode(data) });
    }

    this.config = config;
    const globalContext = this.context().global;
    const knxController = RED.nodes.getNode(config.controller);
    let globalEmitterStatus = this.config.externalEvents.enable;

    if (knxController.config.incomingWrite && !knxController.esfdata) {
      this.warn("Routing functions is enabled but esf file is not loaded, please upload file to use this.");
    }

    knxController.on("status", (status) => {
      this.status({ ...status });
      if (globalEmitterStatus) { globalEmitter(status) }
    });

    knxController.on("traffic", (status) => {
      this.status({ ...status });
    });

    knxController.on("error", (status) => {
      this.status({ ...status });
      if (globalEmitterStatus) { globalEmitter(status) }
    });

    if (!knxController) this.error("Not found knx controller for this node");

    knxController.on("data", (msg) => {
      this.send(msg);
    });

    this.on("input", (msg) => {

      if (!msg && !msg.payload) {
        this.error("Incomming msg don't have payload");
        return;
      }

      if (knxController && knxController.interface) {
        switch (msg.topic) {
          case 'write':
            if (msg.payload.dpt !== null || msg.payload.dpt !== undefined) {
              knxController.interface.write(msg.payload.dstgad, msg.payload.value, msg.payload.dpt);
            } else {
              knxController.interface.write(msg.payload.dstgad, msg.payload.value);
            }
            break;
          case 'read':
            knxController.interface.read(msg.payload.dstgad, (src, responseValue) => this.send({ payload: { dstgad: src, value: responseValue, event: "GroupValue_Read" } }));
            break;
          case 'incomming':
            if (knxController.config.incomingWrite) {
              if (!knxController.esfdata) { return this.error(`Can't find esf file to use funtion routing, please upload and try again or try write with msg.topic = 'raw'`); }
              const element = knxController.esfdata.find(e => e.dg === msg.payload.dstgad);
              if (element) {
                if (element.dpt.startsWith("1")) {
                  knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, 1);
                } else if (element.dpt.startsWith("2")) {
                  knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, 2);
                } else if (element.dpt.startsWith("3")) {
                  knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, 4);
                } else {
                  if (element.dpt === "Uncertain") return this.warn(`DPT is 'Uncertain', please replace for a valid dpt type for routing dg:${msg.payload.dstgad}`);
                  knxController.interface.write(msg.payload.dstgad, valueToString(msg.payload.value), element.dpt);
                }
                // if (element.dpt === "1" || element.dpt === "1.001" || element.dpt === "1.002" || element.dpt === "1.003" && valueToInt(msg.payload.value) === 1) {
                //   knxController.interface.write(msg.payload.dstgad, true, element.dpt);
                // } else if (element.dpt === "1" || element.dpt === "1.001" || element.dpt === "1.002" || element.dpt === "1.003" && valueToInt(msg.payload.value) === 0) {
                //   knxController.interface.write(msg.payload.dstgad, false, element.dpt);
                // } else {
                //   if (element.dpt === "Uncertain") return this.warn(`DPT is 'Uncertain', please replace for a valid dpt type for routing dg:${msg.payload.dstgad}`);
                //   knxController.interface.write(msg.payload.dstgad, valueToString(msg.payload.value), element.dpt);
                // }
              } else {
                //knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value);
                //knxController.interface.write(msg.payload.dstgad, valueToInt(msg.payload.value), msg.payload.dpt);
                return this.warn(`DG: ${msg.payload.dstgad} not find in file .esf, cant't write`);
              }
            } else {
              return this.send(msg);
            }
            break;
          case 'raw':
            // if (msg.payload.bit) {
            //   knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, msg.payload.bit);
            // } 
            if (msg.payload.dpt) {
              if (msg.payload.dpt.startsWith("1")) {
                knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, 1);
              } else if (msg.payload.dpt.startsWith("2")) {
                knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, 2);
              } else if (msg.payload.dpt.startsWith("3")) {
                knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value, 4);
              } else {
                knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value);
              }
            } else {
              knxController.interface.writeRaw(msg.payload.dstgad, msg.payload.value);
            }
            break;
          default:
            this.warn(`Not supported action: '${msg.topic}' inside msg.topic, see help menu.`);
            break;
        }
      }
    });
  }

  function knxDecodeNode(config) {

    RED.nodes.createNode(this, config);

    this.config = config;

    this.on("input", (msg) => {

      if (!msg && !msg.payload) {
        this.error("Incomming msg don't have payload");
        return;
      }

      if (!msg.payload.dpt) {
        this.warn("Can't decode value because don't have dpt into msg.dpt");
        return;
      }

      if (!msg.payload.value) {
        this.warn("Can't decode value because don't have value into msg.value");
        return;
      }

      const decode = {
        ...msg.payload,
        raw: msg.payload.value,
        value: knxDatapoints.decode(msg.payload.dpt, msg.payload.value)
      }

      return this.send({ ...decode });

    })

  }

  RED.nodes.registerType("knx-decode", knxDecodeNode);
  RED.nodes.registerType("knx-controller", knxControllerNode);
  RED.nodes.registerType("knx-interface", knxNode);

}